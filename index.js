// Import main app server module

const appServer = require('./appserver');

// Port number can be configured via an environment
// variable, or will default to port 8080.

const port = process.env.PORT || 8080;

// Start main server. This will initialize
// the database and then start listening
// for connections.

const server = new appServer();
server.start(port);
