FROM node:carbon

WORKDIR /usr/src/app

# Install application dependencies
COPY package*.json ./
COPY README.md ./

# Install product data and server file (node_modules directory
# is explicitly ignored via .dockerignore)
COPY . .

# Install npm modules
RUN npm install

# Application opens port 8080 by default,
# so we'll expose that.
EXPOSE 8080

# Start up node!
CMD ["npm", "start"]
