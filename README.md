# Stackline SDE (Backend) Assessment

This docker image contains an implementation of the Stackline SDE (Backend) Assessment challenge

## Implementation details

This implementation of the API is built in Node.js using the Express.js framework to process HTTP requests. It is split into three separate source files:

* index.js - Main application entry point. This initializes and starts the application server.
* appserver.js - Application server class. This file contains the main application class, which contains the Express server as well as the product information that has been parsed from TSV into JSON objects.
* requests.js - This contains the HTTP routes, which perform the "business logic" of handling the API requests.

## Running on local machine

Run on local machine with the following commands:

`npm install`
`npm start`

## Building docker image

Build the Docker image with the following command:

`docker build .`

## Running within docker

The service will listen for connections on port 8080 by default (This can be overridden by defining the PORT environment variable). The image may be run and connected to on port 8080 with the following command:

`docker run -p 8080:8080 [IMAGE_ID]`

Where IMAGE_ID is the local ID of the image, loaded from a tar or built on your machine. Assuming port 8080, the following routes will be available:

* GET at http://localhost:8080/api/product/autocomplete?type=brand&searchTermPrefix=samsung (example)
* POST at http://localhost:8080/api/product/search

## Built With

* [Node.js](https://nodejs.org/) - The framework used
* [Express.js](https://maven.apache.org/) - Used to handle HTTP requests
* [body-parser](https://www.npmjs.com/package/body-parser) - Extension for Express.js to handle parsing JSON requests
* [tsv](https://www.npmjs.com/package/tsv) - Used to process the tsv product information

## Authors

* **Shane Parker**
