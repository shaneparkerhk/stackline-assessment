// Module imports

const express = require('express');
const requests = require('./requests').Requests;
const bodyParser = require('body-parser');
const fs = require('fs');

const tsv = require('tsv').TSV;
tsv.header = false;

// Main server class. This manages the Express HTTP server
// as well as the main product database.

class AppServer {
	constructor() {
		this._httpserver = null;
		this._products = [];
	}

	_loadProducts(done) {
		// Parse product information from TSV file
		// into the map.

		console.log("Loading product information...");

		fs.readFile("products.tsv", "utf8", (err, data) => {
			const parsed = tsv.parse(data);

			// Format of product information taken from example
			// output defined in email...

			parsed.forEach( (v) => {
				// Looks like some entries in the tsv file come in
				// incomplete, so we check for that here.

				if ( v[0] == '' )
					return;

				// We model the raw TSV data into a structured
				// object here (Structure is based on example
				// API result output provided in assessment
				// email).

				this._products.push({
					"productId": v[0].toString(),
		     		"title": v[1].toString(),
		      		"brandId": v[2].toString(),
		      		"brandName": v[3].toString(),
		      		"categoryId": v[4].toString(),
		      		"categoryName": v[5].toString(),
				});
			});

			// Log number of loaded entries

			console.log("Loaded " + this._products.length + " product entries!");

			// Now that product info is loaded, issue the
			// completion callback.

			done();
		});
	}

	start(port) {
		// Initialize express HTTP server

		this._httpserver = express();

		// Setup parsers for input requests

		this._httpserver.use(bodyParser.json());

		// Add routes for product API

		this._httpserver.get('/api/product/autocomplete', (req, res) => requests.autocomplete(this, req, res));
		this._httpserver.post('/api/product/search', (req, res) => requests.search(this, req, res));

		// Load product information and then start listening
		// once completed.

		this._loadProducts( () => {
			this._httpserver.listen(port, () => console.log("Service listening on port " + port));
		});
	}

	getProducts() {
		return this._products;
	}
};

// Export the AppServer class

module.exports = AppServer;
