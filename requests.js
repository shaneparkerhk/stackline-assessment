// Set of valid field names for products

const fieldNames = ["productId", "title", "brandId", "brandName", "categoryId", "categoryName"];

// Autocomplete suggestion options

const autocompleteTypes = {
	"title": {"fieldName": "title", "idField": "productId", "nameField": "title"},
	"brand": {"fieldName": "brandName", "idField": "brandId", "nameField": "brandName"},
	"category": {"fieldName": "categoryName", "idField": "categoryId", "nameField": "categoryName"}
};

// Class that defines the request handlers

class Requests {
	// Autocomplete request

	static autocomplete(server, req, res) {
		const otype = req.query.type;
		const osearchTermPrefix = req.query.searchTermPrefix;

		// Make sure we have the required parameters

		if ( !otype || !osearchTermPrefix ) {
			res.status(400).send("Invalid query");
			return;
		}

		const type = autocompleteTypes[otype.trim()];
		if ( !type ) {
			res.status(400).send("Invalid autocomplete type: " + otype);
			return;
		}

		const searchTermPrefix = osearchTermPrefix.toLowerCase().trim();
		const suggestions = [];

		// Get product database from server

		const products = server.getProducts();

		// Search products and add ones that match the search
		// criteria specified by the user. We also check for
		// duplicates here using a simple hash map.

		const dups = {};

		products.forEach( (p) => {
			if ( (p[type.fieldName].toLowerCase().indexOf(searchTermPrefix) >= 0) ) {
				const id = p[type.idField];
				const name = p[type.nameField];

				if ( dups[id] )
					return;

				dups[id] = true;

				suggestions.push({
					"id": id,
					"name": name
				});
			}
		});

		// Send the response!

		res.send({
			"type": otype,
			"suggestions": suggestions
		});
	}

	// Search request

	static search(server, req, res) {
		// Make sure we have a valid request...

		if ( !req.body ) {
			res.status(400).send("Invalid query body");
			return;
		}

		// Get the conditions and pagination information

		const conditions = req.body.conditions;
		if ( !Array.isArray(conditions) || (conditions.length == 0) ) {
			res.status(400).send("Invalid query: no conditions provided.");
			return;
		}

		// Get pagination info, or use default of 0-50 
		// if none specified

		const pagination = req.body.pagination || {"from": 0, "size": 50};

		// Check sanity of pagination information

		if ( (pagination.from  === undefined) || (pagination.from < 0) )
			pagination.from = 0;
		if ( (pagination.size === undefined) || (pagination.size > 50) )
			pagination.size = 50;

		// Combine search conditions into a map. This compacts search conditions
		// so that duplicate fieldNames are merged together, etc. We also do some
		// cleaning up of the search terms here.

		const searchTerms = {};

		conditions.forEach( (e) => {
			const fieldName = e.fieldName;
			const values = e.values;

			if ( !fieldName || !values ) {
				res.status(400).send("Invalid query: Invalid condition list format.");
				return;
			}

			if ( fieldNames.indexOf(fieldName) < 0 ) {
				res.status(400).send("Invalid query: Invalid field name '" + fieldName + "'");
				return;
			}

			const m = searchTerms[fieldName] || [];

			searchTerms[fieldName] = m.concat(values.map( (v) => {
				return v.toLowerCase().trim();
			}));
		});

		// Get products database from server

		const products = server.getProducts();

		// Search products and add ones that match the search
		// criteria for the specified fields.

		const results = [];

		products.forEach( (p) => {
			if ( this._productMatches(p, searchTerms) )
				results.push(p);
		});

		// Apply pagination to the results.

		const fresults = results.slice(pagination.from, pagination.size);

		// Send the response!

		res.send({
			"products": fresults,
			"pagination": {
				"from": pagination.from,
				"size": fresults.length
			}
		});
	}

	// Checks if a product matches a given search criteria.
	// This implementation checks to ensure that ALL values
	// within the criteria field are matched. Another implementation
	// could do an 'or' search, but this was not specified
	// in the requirements for this assessment.

	static _productMatches(product, criteria) {
		for ( const k in criteria ) {

			const prop = product[k].toLowerCase();
			const vals = criteria[k];

			for ( const v in vals ) {
				if ( prop.indexOf(vals[v]) < 0 )
					return false;
			}
		}

		return true;
	}
}

// Export the Requests class

module.exports.Requests = Requests;
